const defaults = (obj, defaultProps) => {
 
  if (!obj) return {};
  if (!defaultProps) return obj;

  for (let key in defaultProps) {
    if (!obj[key]) {
      obj[key] = defaultProps[key];
      break;
    }
  }

  return obj;
};

module.exports = defaults;


const keys = (obj) => {
  

  if (!obj) return [];

  const toReturn = [];

  for (let key in obj) {
    toReturn.push(key);
  }

  return toReturn;
};

module.exports = keys;


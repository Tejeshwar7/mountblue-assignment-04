const values = (obj) => {
  

  if (!obj) return [];

  const toReturn = [];
  for (let key in obj) {
    toReturn.push(obj[key]);
  }

  return toReturn;
};

module.exports = values;


const mapObject = (obj, cb) => {

  if (!obj || !cb) return {};

  const toReturn = { ...obj };

  for (let key in obj) {
    toReturn[key] = cb(obj[key], key);
  }

  return toReturn;
};

module.exports = mapObject;


function invert(obj) {
 
  if (!obj) return {};

  const toReturn = {};

  for (let key in obj) {
    toReturn[obj[key]] = key;
  }

  return toReturn;
}

module.exports = invert;


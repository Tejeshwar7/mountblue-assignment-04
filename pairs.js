const pairs = (obj) => {
 
  if (!obj) return [];

  const toReturn = [];

  for (let key in obj) {
    toReturn.push([key, obj[key]]);
  }

  return toReturn;
};

module.exports = pairs;

